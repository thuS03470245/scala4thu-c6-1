package apps

import cc.ListNode

/**
  * Created by mac007 on 2017/4/24.
  */
object ListNodeApp extends App{
val listNode=ListNode(1,ListNode(2,ListNode(3,ListNode(4,null))))
  println(listNode) //1->2->3->4
}
object ListNodeApp1 extends App{
  val listNode=ListNode(1,ListNode(2,ListNode(3,ListNode(4,null))))
  println(listNode.size) //4
}
object ListNodeApp2 extends App{
  val listNode=ListNode(1,ListNode(2,ListNode(3,ListNode(4,null))))
  println(listNode.filter(_!=3)) //1->2->4
}